const express = require("express")
const PenggunaRouter = require("./Router/users")
const BlogRouter = require("./Router/Blog")

const app = express()
app.use(express.json());
app.use("/users", PenggunaRouter)

app.use("/", BlogRouter)

app.listen((3000), () => {
    console.log("Server is Running")
})