const jwt = require ('jsonwebtoken');

const isAuthenticated = (req, res, next) =>{
    const authorization = req.header['Authorization'] || req.header['authorization'];

    if(authorization == "" || typeof authorization === 'undefined') {
        return res.sendStatus(401)
    }

    try {
            const isVerified = jwt.verify(authorization, "SomeSecret", {
        algorithms: "HS256",
        })

        const decodeuserData = jwt.decode(authorization, {
            json: true
        })
        
        req.userData = {
            id: decodeuserData.id,
            name:decodeuserData.name,
            namaLengkap: decodeuserData.namaLengkap
        };

    } catch (error) {
        return res.sendStatus(401)
    }

    next()
}

module.exports = isAuthenticated