const FastestValidator = require('fastest-validator');

const formValidation = new FastestValidator({
    useNewCustomCheckerFunction: true,
    messages: {
        titleHarusDiisi: "title Tidak Boleh Kosong",
        contentHarusDiisi: "content Tidak Boleh Kosong"
    }
});

module.exports = function (req, res, next) {

    const schema = {
        title: {
            type: "string",
            custom: (v, errors) => {
                if ( v == '') {
                    errors.push({ type: "titleHarusDiisi" })
                    return
                }
                return v;
            },
        },
        content: {
            type: "string",
            custom: (v, errors) => {
                if (v == '') {
                    errors.push({ type: "contentHarusDiisi" })
                    return
                }
                return v;
            },
        },
    }


    const isValidate = formValidation.validate(req.body, schema)


    if (isValidate !== true) {
        return res.status(400).send(isValidate)
    } else {
        return next();
    }
}
